---
---

const loadEvent = () => {
  try {
    if (Turbolinks) return 'turbolinks:load'
  } catch {
    return 'DOMContentLoaded'
  }
}

document.addEventListener(loadEvent(), () => {
  document.querySelectorAll('.share').forEach(share => {
    share.addEventListener('click', event => {
      if (!Navigator.share) return;

      event.preventDefault();
      event.stopPropagation();

      const title = document.querySelector('title').text;

      try {
        const text = document.querySelector('meta[property="og:description"]').content;
      } catch {
        const text = '';
      }

      try {
        const url = document.querySelector('link[rel=canonical]').href;
      } catch {
        const url = document.location.href;
      }

      const data = { title: title, text: text, url: url };

      if (Navigator.canShare(data)) Navigator.share(data).then();
    });
  });

  document.querySelectorAll("a[href^='http://'],a[href^='https://'],a[href^='//']").forEach(a => {
    a.rel = "noopener";
    a.target = "_blank";
  });
});
