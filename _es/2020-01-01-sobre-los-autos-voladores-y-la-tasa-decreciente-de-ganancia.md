---
draft: false
categories: []
seo:
  type: CreativeWork
layout: book
title: Sobre los autos voladores y la tasa decreciente de ganancia
author:
- David Graeber
description: |
  Una pregunta secreta flota sobre nosotras, una sensación de decepción,
  una promesa rota que nos fue hecha de niñas sobre cómo sería el mundo
  al convertirnos en adultas.
image:
  path: public/sobre-los-autos-voladores-y-la-tasa-decreciente-de-ganancia.png
  description: Sobre los autos voladores y la tasa decreciente de ganancia
width: 100
height: 140
depth: 5
pages: 50
price: 120
uuid: a9724e2f-6927-40c6-9a19-1b5fb0a81ee0
sku: a9724e2f-6927-40c6-9a19-1b5fb0a81ee0
tags: []
weight: '0.0'
cost_price:
in_stock: false
variant_id: 2
---

Una pregunta secreta flota sobre nosotras, una sensación de decepción,
una promesa rota que nos fue hecha de niñas sobre cómo sería el mundo al
convertirnos en adultas. No me refiero a las falsas promesas estándar
que siempre nos hacen a las niñas sobre cómo el mundo es justo o cómo
aquellas que se esfuercen más serán las mejor recompensadas, sino a la
promesa generacional particular –hecha a las que fuimos niñas en los
cincuenta, sesenta, setenta y ochenta– que nunca fue articulada como
tal, más bien hecha como un conjunto de suposiciones respecto a cómo
sería nuestro mundo adulto. Y como nunca fue realmente prometida, ahora
que no se ha convertido en realidad, nos hemos quedado confundidas:
indignadas, pero al mismo tiempo avergonzadas de nuestra propia
indignación; avergonzadas, en primer lugar, de haber sido tan tontas de
creerle a nuestros mayores.

En concreto, ¿dónde están los autos voladores? ¿Dónde están los campos
de fuerza, los rayos atractores, los podios de teletransportación, los
trineos antigravitacionales...
