---
draft: false
categories: []
seo:
  type: CreativeWork
layout: book
title: Sobre los autos voladores y la tasa decreciente de ganancia
author:
- David Graeber
description: |
  Una pregunta secreta flota sobre nosotras, una sensación de decepción,
  una promesa rota que nos fue hecha de niñas sobre cómo sería el mundo
  al convertirnos en adultas.
image:
  path: public/sobre-los-autos-voladores-y-la-tasa-decreciente-de-ganancia.png
  description: Sobre los autos voladores y la tasa decreciente de ganancia
width: 100
height: 140
depth: 5
pages: 50
price: 120
stock: 100
uuid: af77e701-e2e6-4192-b69b-b034be42c300
sku: af77e701-e2e6-4192-b69b-b034be42c300
tags: []
variant_id: 3
weight: '0.0'
cost_price:
in_stock: true
---

Una pregunta secreta flota sobre nosotras, una sensación de decepción,
una promesa rota que nos fue hecha de niñas sobre cómo sería el mundo al
convertirnos en adultas. No me refiero a las falsas promesas estándar
que siempre nos hacen a las niñas sobre cómo el mundo es justo o cómo
aquellas que se esfuercen más serán las mejor recompensadas, sino a la
promesa generacional particular –hecha a las que fuimos niñas en los
cincuenta, sesenta, setenta y ochenta– que nunca fue articulada como
tal, más bien hecha como un conjunto de suposiciones respecto a cómo
sería nuestro mundo adulto. Y como nunca fue realmente prometida, ahora
que no se ha convertido en realidad, nos hemos quedado confundidas:
indignadas, pero al mismo tiempo avergonzadas de nuestra propia
indignación; avergonzadas, en primer lugar, de haber sido tan tontas de
creerle a nuestros mayores.

En concreto, ¿dónde están los autos voladores? ¿Dónde están los campos
de fuerza, los rayos atractores, los podios de teletransportación, los
trineos antigravitacionales...
