---
seo:
  type: CreativeWork
layout: book
title: Sobre los autos voladores y la tasa decreciente de ganancia
author:
- David Graeber
description: |
  Una pregunta secreta flota sobre nosotras, una sensación de decepción,
  una promesa rota que nos fue hecha de niñas sobre cómo sería el mundo
  al convertirnos en adultas.
image:
  path: public/sobre-los-autos-voladores-y-la-tasa-decreciente-de-ganancia.png
  description: Sobre los autos voladores y la tasa decreciente de ganancia
size: 10x14cm.
pages: 50
availability: true
price: 120
currency: ARS
delivery_methods:
- direct_download
- pick_up
- mail
payment_methods:
- cash
- direct_debit
- bank_transfer_in_advance
payment_url: https://www.mercadopago.com.ar/checkout/v1/redirect/238f11e0-361a-4c17-b6c0-3319f28e0d33/payment-option-form/?preference-id=440784774-3c17ff2d-b48d-4514-99c0-993fb3e5ad59&p=1071e8d1d2cef5ab3a355c0f47ccc644
---

Una pregunta secreta flota sobre nosotras, una sensación de decepción,
una promesa rota que nos fue hecha de niñas sobre cómo sería el mundo al
convertirnos en adultas. No me refiero a las falsas promesas estándar
que siempre nos hacen a las niñas sobre cómo el mundo es justo o cómo
aquellas que se esfuercen más serán las mejor recompensadas, sino a la
promesa generacional particular –hecha a las que fuimos niñas en los
cincuenta, sesenta, setenta y ochenta– que nunca fue articulada como
tal, más bien hecha como un conjunto de suposiciones respecto a cómo
sería nuestro mundo adulto. Y como nunca fue realmente prometida, ahora
que no se ha convertido en realidad, nos hemos quedado confundidas:
indignadas, pero al mismo tiempo avergonzadas de nuestra propia
indignación; avergonzadas, en primer lugar, de haber sido tan tontas de
creerle a nuestros mayores.

En concreto, ¿dónde están los autos voladores? ¿Dónde están los campos
de fuerza, los rayos atractores, los podios de teletransportación, los
trineos antigravitacionales...
