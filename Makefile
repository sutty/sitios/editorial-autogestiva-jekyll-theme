SHELL := bash
.DEFAULT_GOAL := all

site ?= $(patsubst %-jekyll-theme,%,$(notdir $(PWD)))
domain ?= $(site).sutty.local
env ?= production
JEKYLL_ENV ?= $(env)

include .env

export

all: fa build ## Todo

build: ## Compilar el sitio
	bundle exec jekyll build --profile --trace

serve: /etc/hosts kill-servers ## Servidor de prueba
	@echo "Iniciando servidor web en https://$(domain):4000/"
	@if ! ss -lnp | grep -q :65000 ; then nghttpd -d _site/ 65000 ../sutty.local/domain/sutty.local.{key,crt} & echo $$! > /tmp/nghttpd.pid ; fi
	@if ! ss -lnp | grep -q :4000 ; then nghttpx -b "127.0.0.1,65001;/assets/js/:/sockjs-node/" -b "$(domain),65000;/;proto=h2;tls" -f "0.0.0.0,4000" -L FATAL --no-ocsp ../sutty.local/domain/sutty.local.{key,crt} & echo $$! > /tmp/nghttpx.pid ; fi

kill-servers: /tmp/nghttpd.pid /tmp/nghttpx.pid /tmp/webpack.pid ## Cerrar el servidor

webpack: assets/js/pack.js ## Compilar JS

webpack-dev-server: /tmp/webpack.pid ## Servidor de prueba de Webpack
	./node_modules/.bin/webpack-dev-server --public $(domain):4000 --host 127.0.0.1 --port 65001 & echo $$! > /tmp/webpack.pid

# Tomar los códigos de los íconos de este archivo y copiarlos a fa.txt
# node_modules/font-awesome/scss/_variables.scss
assets/fonts/forkawesome-webfont.woff2: fa.txt
	which glyphhanger || npm i -g glyphhanger
	grep -v "^#" fa.txt | sed "s/^/U+/" | tr "\n" "," | xargs -rI {} glyphhanger --subset=node_modules/fork-awesome/fonts/forkawesome-webfont.ttf --formats=woff2 --whitelist="{}"
	mv node_modules/fork-awesome/fonts/forkawesome-webfont-subset.woff2 $@

fa: assets/fonts/forkawesome-webfont.woff2 ## Fork Awesome

push: ## Publica los cambios locales
	sudo chgrp -R 82 _site
	rsync -avi --delete-after _site/ root@athshe.sutty.nl:/srv/sutty/srv/http/data/_deploy/$(site).sutty.nl/

/etc/hosts: always
	@echo "Chequeando si es necesario agregar el dominio local $(domain)"
	@grep -q " $(domain)$$" $@ || echo -e "127.0.0.1 $(domain)\n::1 $(domain)" | sudo tee -a $@

%.pid: always
	@test -f $@ && cat $@ | xargs -r kill &>/dev/null || :
	@rm -f $@

js = $(wildcard _packs/*.js) $(wildcard _packs/*/*.js) $(wildcard *.js)
assets/js/pack.js: $(js)
	./node_modules/.bin/webpack --config webpack.prod.js

.PHONY: always
