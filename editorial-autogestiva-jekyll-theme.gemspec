# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'editorial-autogestiva-jekyll-theme'
  spec.version       = '0.3.4'
  spec.authors       = ['f']
  spec.email         = ['f@sutty.nl']

  spec.summary       = 'Una plantilla para editoriales autogestivas'
  spec.homepage      = 'https://0xacab.org/sutty/jekyll/editorial-autogestiva-jekyll-theme'
  spec.license       = 'Nonstandard'

  spec.files         = Dir['assets/**/*',
                           '_layouts/**/*',
                           '_includes/**/*',
                           '_sass/**/*',
                           '_data/**/*',
                           '_config.yml',
                           'LICENSE*',
                           'README*']

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  spec.metadata = {
    'bug_tracker_uri'   => "#{spec.homepage}/issues",
    'homepage_uri'      => spec.homepage,
    'source_code_uri'   => spec.homepage,
    'changelog_uri'     => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  # Dependencias, esto se va a instalar al instalar la gema
  spec.add_runtime_dependency 'jekyll', '~> 4'
  spec.add_runtime_dependency 'jekyll-relative-urls', '~> 0'
  spec.add_runtime_dependency 'jekyll-seo-tag', '~> 2'
  spec.add_runtime_dependency 'jekyll-images', '~> 0.2'
  spec.add_runtime_dependency 'jekyll-include-cache', '~> 0'
  spec.add_runtime_dependency 'jekyll-data', '~> 1.1'
  spec.add_runtime_dependency 'jekyll-locales', '~> 0.1'
  spec.add_runtime_dependency 'jekyll-linked-posts', '~> 0'
  spec.add_runtime_dependency 'jekyll-order', '~> 0'
  spec.add_runtime_dependency 'sutty-liquid', '~> 0'
  spec.add_runtime_dependency 'jekyll-commonmark', '~> 1.3'
  spec.add_runtime_dependency 'jekyll-dotenv', '>= 0.2'
  spec.add_runtime_dependency 'jekyll-feed', '~> 0.15'
  spec.add_runtime_dependency 'jekyll-spree-client', '~> 0'
  spec.add_runtime_dependency 'jekyll-write-and-commit-changes', '~> 0'
  spec.add_runtime_dependency 'jekyll-ignore-layouts', '~> 0'
  spec.add_runtime_dependency 'jekyll-hardlinks', '~> 0'
  spec.add_runtime_dependency 'jekyll-unique-urls', '~> 0'

  spec.add_development_dependency 'bundler', '~> 2.1'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'pry'
end
